from django.shortcuts import render
from groups.models import Group
from django.http import HttpResponse ,JsonResponse
from django.core.serializers import serialize
from django.contrib.auth.decorators import login_required
import json

def group_view(request):
    if request.method == "GET":
        groups = serialize("json",Group.objects.all());
        print(groups)
        return JsonResponse({
            "type": "ok",
            "status" : 200,
            "data":json.loads(groups)
        },safe=False)
    elif request.method=="POST":
        name = request.POST["name"]
        group   = Group.objects.create(name=name)
        return JsonResponse({
            "type": "ok",
            "status" : 200,
            "message": "Group saved successfully",
            "data" : json.loads(serialize("json",[Group.objects.get(pk=group.pk)]))
        },safe=False)