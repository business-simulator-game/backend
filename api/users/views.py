from django.http import JsonResponse, HttpResponse
from django.contrib.auth.models import User
from django.contrib.auth import login, logout,authenticate
from django.core.serializers import serialize
from rest_framework.decorators import api_view
from django.contrib.auth.decorators import login_required
import json

@api_view(['GET', 'POST'])
def user_view(request):
    if request.method == "GET":
        data = User.objects.all()
        data = serialize("json",data)
        return HttpResponse(data,content_type="application/json");
    elif request.method == "POST":
        email = request.POST["email"]
        username = request.POST["username"]
        password = request.POST["password"]
        group_id = request.POST["group_id"]
        user = User.objects.create_user(
            email=email,
            username=username,
            password=password,
            group_id=group_id,
            is_staff=False,
            is_superuser=False,
            );
        if user:
            return JsonResponse({
                "type": "ok",
                "status" : 200,
                "message": "User saved successfully"
            },safe=False)
        else:
            return JsonResponse({
                "type": "error",
                "status" : 500,
                "message": "User not saved"
            },safe=False,status=500)

@api_view(["POST"])
def login_view(request):
    username = request.POST["username"]
    password = request.POST["password"]
    user = authenticate(request,username=username,password=password)

    if user:
        login(request,user)
        user = User.objects.get(pk=user.pk);
        user = {
            "id":user.pk,
            "username":user.username,
            "email":user.email,
            "is_admin":user.is_staff,
        }
        return JsonResponse({
            "type": "ok",
            "status" : 200,
            "message": "Loggin successfully",
            "user":user
        },safe=False)
    else:
        return JsonResponse({
            "type": "error",
            "status" : 404,
            "message": "Bad credentials"
        },safe=False,status=404)


@api_view(["GET"])
def logout_view(request):
    logout(request);
    return JsonResponse({
            "type": "ok",
            "status" : 200,
            "message": "Session closed"
        },safe=False)

