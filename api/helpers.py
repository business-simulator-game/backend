from django.core.serializers import serialize
from django.http import JsonResponse
import json


def toJson(objects, is_once=True):
    if is_once:
        return json.loads(serialize("json", [objects]))
    else:
        return json.loads(serialize("json", objects))


def Response(message, types="ok", code=200, data=[]):

    if(len(data) == 0):
        return JsonResponse({
            "type": types,
            "status": code,
            "message": message,
        }, safe=False, status=code)
    else:
        return JsonResponse({
            "type": types,
            "status": code,
            "message": message,
            "data": data,
        }, safe=False, status=code)


def validateNumber(dato):
    if dato == "":
        return 0
    num = dato.replace(".", "")
    return int(num)
