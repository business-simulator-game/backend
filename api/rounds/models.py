from django.db import models

from games.models import Game
class Round(models.Model):
    game = models.ForeignKey(Game,on_delete=models.CASCADE)
    time_seconds = models.IntegerField()
    is_end = models.BooleanField()