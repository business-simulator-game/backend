from django.contrib import admin

from participations.models import Participation
admin.site.register(Participation)
