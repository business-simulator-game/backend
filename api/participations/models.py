from django.db import models
from django.contrib.auth.models import User
from rounds.models import Round
class Participation(models.Model):
    user = models.ForeignKey(User,on_delete=models.CASCADE)
    round = models.ForeignKey(Round,on_delete=models.CASCADE)
    total_budget = models.DecimalField(max_digits=50, decimal_places=2)
    price_bottle_sell = models.DecimalField(max_digits=50, decimal_places=2)
    price_barrel_sell = models.DecimalField(max_digits=50, decimal_places=2)
    quantity_bottle = models.IntegerField()
    quantity_barrel = models.IntegerField()
    spend_machine = models.DecimalField(max_digits=50, decimal_places=2)
    spend_personal = models.DecimalField(max_digits=50, decimal_places=2)
    spend_supplies = models.DecimalField(max_digits=50, decimal_places=2)
    spend_marketing = models.DecimalField(max_digits=50, decimal_places=2)

