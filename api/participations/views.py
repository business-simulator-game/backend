from django.core.serializers import serialize
from rest_framework.decorators import api_view
from django.contrib.auth.models import User
from rounds.models import Round
from participations.models import Participation
from games.models import Game, GroupGame
from groups.models import Group
from helpers import Response, validateNumber, toJson
import functools
from decimal import *
class HelpParticipation:

    @staticmethod
    def canParticipateUser(game_id,user_id,registers = False,lastBudget= False):
        game = Game.objects.get(pk=game_id)
        user = User.objects.get(pk=user_id)
        rounds =  list(Round.objects.filter(game_id=game_id).values_list("pk",flat=True))
        participations = Participation.objects.filter(round_id__in=rounds,user_id=user_id)
        if lastBudget:
            if participations.last():
                return participations.last().total_budget
            else:
                return False

        if registers:
            return participations
        return participations.count()

    @staticmethod
    def getIdParticipationRound(game_id,user_id):
        round_ids = list(Round.objects.filter(game_id=game_id).values_list("pk",flat=True))
        matchs_ids = list(Participation.objects.filter(round_id__in=round_ids,user_id=user_id).values_list("round_id",flat=True))
        options = []
        data = {}
        i = 0
        for item in round_ids:
            try:
                if(round_ids[i] != matchs_ids[i]):
                    options.append(round_ids[i])
            except:
                options.append(round_ids[i])
            i = i + 1
        data.setdefault("length",len(options))
        if len(options) > 0:
            data.setdefault("round_id",options[0])
        else:
            data.setdefault("round_id",None)
        return data

    @staticmethod
    def getAllParticipationsByGroup(game_id):
        def avg(list):
            if len(list) > 0:
                return sum(list) / len(list)
            else:
                return 0
        def addInDict(info):
            return {
                    "total_budget":Decimal(round(avg(info["total_budget"]),2)),
                    "price_bottle_sell":Decimal( round(avg(info["price_bottle_sell"]),2)),
                    "price_barrel_sell":Decimal( round(avg(info["price_barrel_sell"]),2)),
                    "quantity_bottle":Decimal( round(avg(info["quantity_bottle"]),2)),
                    "quantity_barrel": Decimal(round(avg(info["quantity_barrel"]),2)),
                    "spend_machine": Decimal(round(avg(info["spend_machine"]),2)),
                    "spend_personal":Decimal( round(avg(info["spend_personal"]),2)),
                    "spend_supplies": Decimal(round(avg(info["spend_personal"]),2)),
                    "spend_marketing":Decimal( round(avg(info["spend_marketing"]),2)),
                }
        def newTemplateDict():
            return  {
                "total_budget":[],
                "price_bottle_sell": [],
                "price_barrel_sell": [],
                "quantity_bottle": [],
                "quantity_barrel": [],
                "spend_machine":[],
                "spend_personal":[],
                "spend_supplies":[],
                "spend_marketing": [],
            }

        """
           Se requiere hacer el calculo de un promedio de cuanto en porcentaje debe ser repartido por los participantes
        """
        group_games_ids = GroupGame.objects.filter(game_id=game_id).values_list("group_id",flat=True)
        data = []

        for id in group_games_ids:
            group = Group.objects.get(pk=id)
            users = User.objects.filter(group_id= id)
            users_a =[]
            game_business = []
            for user in users:
                participations = HelpParticipation.canParticipateUser(game_id,user.pk,True)
                info = newTemplateDict()
                for item in participations:
                    info["total_budget"].append(item.total_budget)
                    info["price_bottle_sell"].append(item.price_bottle_sell)
                    info["price_barrel_sell"].append(item.price_barrel_sell)
                    info["quantity_bottle"].append(item.quantity_bottle)
                    info["quantity_barrel"].append(item.quantity_barrel)
                    info["spend_machine"].append(item.spend_machine)
                    info["spend_personal"].append(item.spend_personal)
                    info["spend_personal"].append(item.spend_supplies)
                    info["spend_marketing"].append(item.spend_marketing)
                all_data = addInDict(info);
                all_data.setdefault("name",user.username);
                all_data.setdefault("email",user.email);
                all_data.setdefault("participations",len(participations));
                users_a.append(all_data)

            info = newTemplateDict()
            for student in users_a:
                info["total_budget"].append(student["total_budget"])
                info["price_bottle_sell"].append(student["price_bottle_sell"])
                info["price_barrel_sell"].append(student["price_barrel_sell"])
                info["quantity_bottle"].append(student["quantity_bottle"])
                info["quantity_barrel"].append(student["quantity_barrel"])
                info["spend_machine"].append(student["spend_machine"])
                info["spend_personal"].append(student["spend_personal"])
                info["spend_personal"].append(student["spend_personal"])
                info["spend_marketing"].append(student["spend_marketing"])
            all_data = addInDict(info);
            all_data.setdefault("students",len(users_a));
            game_business.append(all_data)

            data.append({
                "id" : id,
                "name": group.name,
                "game_business":game_business,
                "stundents": users_a
            })
        average_price_bottle = avg([it["game_business"][0]["price_bottle_sell"] for it in data])
        average_price_barrel = avg([ite["game_business"][0]["price_barrel_sell"] for ite in data])
        new_data = []
        for item in data:
            data = item["game_business"][0]
            quantity_bottle_sells = data["quantity_bottle"] *( 1 - (data["price_bottle_sell"] / average_price_barrel))
            total_bottle_sells = data["price_bottle_sell"] * quantity_bottle_sells
            item.setdefault("data_statics",{
                "quantity_bottle_sells" : quantity_bottle_sells,
                "total_bottle_sells" : total_bottle_sells,
                "total_bottle_sells_more_budget" : sum([total_bottle_sells,data["total_budget"]]),
                "budget_average":data["total_budget"]
            })
            new_data.append(item.copy())
        #
        # print([ {"hola":23} if max(item["data_statics"]["total_bottle_sells"])  else item for item in new_data])
        return new_data


@api_view(["POST"])
def participation_view(request):
    if request.method == "POST":
        game_id = int(request.POST["game_id"])
        user_id = int(request.POST["user_id"])
        max_rounds = Game.objects.get(pk=game_id).count_rounds
        num_participations = HelpParticipation.canParticipateUser(game_id,user_id)
        if(num_participations >= max_rounds ):
            return Response("You are try to send your participation when you have pass of max : {}".format(max_rounds), "max_round_allowed", 400)
        participation = Participation.objects.create(
            user=User.objects.get(pk=int(request.POST["user_id"])),
            round=Round.objects.get(pk=int(request.POST["round_id"])),
            total_budget=validateNumber(request.POST["budget"]),
            price_bottle_sell=validateNumber(request.POST["price_bottle_sell"]),
            price_barrel_sell=validateNumber(request.POST["price_barrel_sell"]),
            quantity_bottle=validateNumber(request.POST["quantity_bottle_sell"]),
            quantity_barrel=validateNumber(request.POST["quantity_barrel_sell"]),
            spend_machine=validateNumber(request.POST["spend_machine"]),
            spend_personal=validateNumber(request.POST["spend_personal"]),
            spend_supplies=validateNumber(request.POST["spend_supplies"]),
            spend_marketing=validateNumber(request.POST["spend_marketing"]),
        )
        num_participations = HelpParticipation.canParticipateUser(game_id,user_id)
        budget = HelpParticipation.canParticipateUser(game_id,user_id,False,True)
        rounds = Round.objects.filter(game_id=game_id).count()
        round = Round.objects.filter(game_id=game_id).last()
        if num_participations >= max_rounds:
               return Response("good game, you have finished the game", "max_round_allowed", 200)
        # if rounds <= num_participations:
        data_rounds = HelpParticipation.getIdParticipationRound(game_id,user_id)
        if data_rounds["length"] > 0:
            round = Round.objects.get(pk=data_rounds["round_id"])
            return Response("all right", "ok", 200, {
                "user_id": user_id,
                "rounds": num_participations + 1,
                "round_id": round.pk,
                "time": round.time_seconds,
                "budget": budget,
            })
        else:
            return Response("Error al crear las rondas del juego", "error", 500)


