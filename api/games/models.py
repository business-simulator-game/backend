from django.db import models

from groups.models import Group
# Create your models here.
class Game(models.Model):

    id_user_admin = models.IntegerField()
    pin = models.CharField(max_length=6)
    count_rounds = models.IntegerField()
    created = models.DateTimeField(auto_now_add=True)

class GroupGame(models.Model):
    group = models.ForeignKey(Group,on_delete= models.CASCADE)
    game = models.ForeignKey(Game,on_delete= models.CASCADE)
    created = models.DateTimeField(auto_now_add=True)