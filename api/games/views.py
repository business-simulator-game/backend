from django.http import JsonResponse
from django.core.serializers import serialize
from games.models import Game, GroupGame
from groups.models import Group
from rounds.models import Round
from participations.models import Participation
from django.contrib.auth.models import User
from rest_framework.decorators import api_view
from helpers import toJson, Response
import json
from participations.views import HelpParticipation


@api_view(["GET"])
def detail_game_view(request,pin):
    id = Game.objects.filter(pin=pin).first()
    if id :
        data = HelpParticipation.getAllParticipationsByGroup(id.pk)
        if len(list(data)) > 0:
            return Response("all right","ok",200,data)
        else:
            return Response("There is not registers","error",400)
    else:
        return Response("the pin not exists","error",400)


@api_view(["GET", "POST"])
def game_view(request):
    if request.method == "GET":
        games = serialize("json", Game.objects.all().order_by("-pk"))
        games = json.loads(games)

        for game in games:
            user = json.loads(serialize("json", [User.objects.get(
                pk=game["fields"]["id_user_admin"])]))[0]["fields"]
            game["fields"]["user"] = {
                "username": user["username"],
                "email": user["email"],
                "is_staff": user["is_staff"],
            }
        return JsonResponse({
            "type": "ok",
            "status": 200,
            "data": games
        }, safe=False)

    elif request.method == "POST":
        id_user_admin = request.POST["id_user_admin"]
        pin = request.POST["pin"]
        count_rounds = 3
        time_seconds = 300
        game = Game.objects.create(
            id_user_admin=id_user_admin, pin=pin, count_rounds=count_rounds)
        for group in Group.objects.all():
            GroupGame.objects.create(group_id=group.pk, game_id=game.pk)
        for i in range(0,count_rounds):
            Round.objects.create(game_id=game.pk,time_seconds=time_seconds,is_end=False)
        return JsonResponse({
            "type": "ok",
            "status": 200,
            "message": "Group saved successfully",
            "data": json.loads(serialize("json", [Game.objects.get(pk=game.pk)]))
        }, safe=False)


@api_view(["POST"])
def game_join_view(request):
    user_id = request.POST["id_user"]
    pin = request.POST["pin"]
    game = Game.objects.filter(pin=pin).first()
    user = User.objects.filter(pk=user_id).first()
    # rounds = Round.objects.filter(game_id=game.pk)

    max_rounds = Game.objects.get(pk=game.pk).count_rounds
    num_participations = HelpParticipation.canParticipateUser(game.pk,user.pk)
    budget = HelpParticipation.canParticipateUser(game.pk,user.pk,False,True)
    if not budget:
        budget = 1000000000
    if num_participations >= max_rounds :
        return Response("You are finish your round max : {}".format(max_rounds), "error", 404)

    if game and user:
        group_id = user.group_id
        if group_id:
            data_rounds = HelpParticipation.getIdParticipationRound(game.pk,user.pk)
            if data_rounds["length"] > 0:
                round = Round.objects.get(pk=data_rounds["round_id"])
                return Response("all right", "ok", 200, {
                    "game_id": game.pk,
                    "user_id": user.pk,
                    "user_admin_id": game.id_user_admin,
                    "group_id": group_id,
                    "pin": game.pin,
                    "rounds": num_participations + 1,
                    "round_id": round.pk,
                    "time": round.time_seconds,
                    "budget": budget,

                })
            else:
                return Response("Error al crear las rondas del juego", "error", 500)
        else:
            return Response("User not have a group", "error", 404)
    else:
        return Response("Pin or user not found", "error", 404)
