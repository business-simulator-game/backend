
from django.contrib import admin
from django.urls import path , include
from django.http import HttpResponse
from users import views
from games import views as viewsGame
from groups import views as viewsGroup
from rounds import views as viewRound
from participations import views as viewParticipation

def default(request):
    return HttpResponse("Hola Bienvenid@ esta API para juego gerencial.")
urlpatterns = [
    path("participations",viewParticipation.participation_view),
    path("rounds",viewRound.round_view),
    path("users",views.user_view),
    path("groups",viewsGroup.group_view),
    path('games', viewsGame.game_view),
    path('games/detail/<int:pin>', viewsGame.detail_game_view),
    path('games/join', viewsGame.game_join_view),
    # default
    path('logout', views.logout_view),
    path('login', views.login_view),
    path('admin', admin.site.urls),
    path('', default)
]
